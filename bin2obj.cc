#include <algorithm>
#include <ctime>
#include <cstdint>
#include <fstream>
#include <iostream>
#include <iterator>
#include <memory>
#include <string>
#include <vector>

#include <Windows.h>

std::string g_usage = "bin2obj symbol_base input.bin output.obj output.h";

template<typename T>
void
blit(std::ostream& os, T&& x)
{
  os.write((char const*)&x, sizeof(x));
}

template<typename Integer, typename Alignment>
Integer
align(Integer i, Alignment a)
{
  auto rem = i % a;
  if (rem == 0) {
    return i;
  }
  return i + (a - rem);
}

struct SpaceAllocator
{
  uint32_t allocate(uint32_t n)
  {
    auto ret = offset;
    offset += n;
    return ret;
  }

  uint32_t aligned_allocate(uint32_t amount, uint32_t alignment)
  {
    auto ret = align(offset, alignment);
    offset = ret + amount;
    return ret;
  }

  uint32_t offset = 0;
};

struct Variable
{
  virtual ~Variable() {}

  virtual void write_to_stream(std::ostream& os) = 0;
  virtual uint32_t get_size() = 0;
  virtual uint32_t get_align() = 0;
  virtual char const* get_name() = 0;
};

struct StreamVariable : Variable
{
  StreamVariable(std::string name,
                 std::istream& is,
                 uint32_t size,
                 uint32_t align)
    : name(name)
    , is(is)
    , size(size)
    , align(align)
  {
  }

  void write_to_stream(std::ostream& os) override
  {
    std::copy_n(std::istreambuf_iterator<char>(is),
                size,
                std::ostreambuf_iterator<char>(os));
  }

  uint32_t get_size() override { return size; }

  uint32_t get_align() override { return align; }

  char const* get_name() override { return name.c_str(); }

private:
  std::string name;
  std::istream& is;
  uint32_t size;
  uint32_t align;
};

struct OwnedDataVariable : Variable
{
  OwnedDataVariable(std::string name, void* data, uint32_t size, uint32_t align)
    : name(name)
    , data((char const*)data, (char const*)data + size)
    , align(align)
  {
  }

  void write_to_stream(std::ostream& os) override
  {
    os.write(data.data(), data.size());
  }

  uint32_t get_size() override { return data.size(); }

  uint32_t get_align() override { return align; }

  char const* get_name() override { return name.c_str(); }

private:
  std::string name;
  std::vector<char> data;
  uint32_t align;
};

enum class Arch
{
  x86_64,
  i386,
};

bool
is_32bit(Arch arch)
{
  switch (arch) {
    case Arch::x86_64:
      return false;
    case Arch::i386:
      return true;
    default:
      abort();
  }
}

struct CoffFile
{
  struct FileHeader
  {
    uint16_t machine_type;
    uint16_t section_count;
    uint32_t time_date_stamp;
    uint32_t symbol_table_pointer;
    uint32_t symbol_count;
    uint16_t optional_header_size;
    uint16_t characteristics;
  };

  struct SectionTableEntry
  {
    char name[8];
    uint32_t virtual_size;
    uint32_t virtual_address;
    uint32_t size_of_raw_data;
    uint32_t pointer_to_raw_data;
    uint32_t pointer_to_relocations;
    uint32_t pointer_to_line_numbers;
    uint16_t number_of_relocations;
    uint16_t number_of_line_numbers;
    uint32_t characteristics;
  };

  struct SymbolTableEntry
  {
    union
    {
      char short_name[8];
      struct
      {
        uint32_t zeroes, offset;
      };
    } name;
    uint32_t value;
    uint16_t section_number;
    uint16_t type;
    uint8_t storage_class;
    uint8_t number_of_aux_symbols;
  };

  struct StringTable
  {
    uint32_t total_size;
    std::vector<std::string> strings;
  };

  explicit CoffFile(Arch arch)
    : arch(arch)
  {
  }

  void add_variable(std::shared_ptr<Variable> var) { variables.push_back(var); }

  void write_to_stream(std::ostream& os)
  {
    SpaceAllocator file_arena;
    file_arena.allocate(20); // file header

    uint32_t section_table_base = file_arena.allocate(40);

    SpaceAllocator rdata_arena;
    SpaceAllocator string_arena;
    string_arena.allocate(4); // u32 size field
    std::vector<char> string_data(4);
    std::vector<uint32_t> variable_offsets;
    std::vector<SymbolTableEntry> symbol_entries;
    for (auto&& var : variables) {
      auto offset =
        rdata_arena.aligned_allocate(var->get_size(), var->get_align());
      auto name = var->get_name();
      auto name_len = strlen(name);
      variable_offsets.push_back(offset);
      SymbolTableEntry symbol_entry = { 0 };
      if (name_len <= 8) {
        memcpy(symbol_entry.name.short_name, name, name_len);
      } else {
        symbol_entry.name.zeroes = 0;
        symbol_entry.name.offset = string_arena.allocate(name_len + 1);
        string_data.insert(string_data.end(), name, name + name_len + 1);
      }
      symbol_entry.value = offset;
      symbol_entry.section_number = 1;
      symbol_entry.type = 0x00;
      symbol_entry.storage_class = 2; // external
      symbol_entry.number_of_aux_symbols = 0;
      symbol_entries.push_back(symbol_entry);
    }
    memcpy(string_data.data(), &string_arena.offset, sizeof(uint32_t));
    uint32_t symbol_table_base =
      file_arena.allocate(symbol_entries.size() * 18);

    FileHeader file_header = { 0 };
    file_header.machine_type = machine_type(arch);
    file_header.section_count = 1;
    file_header.time_date_stamp = std::time(nullptr);
    file_header.symbol_table_pointer = symbol_table_base;
    file_header.symbol_count = (uint32_t)variables.size();
    file_header.optional_header_size = 0;
    file_header.characteristics =
      IMAGE_FILE_LARGE_ADDRESS_AWARE |
      (is_32bit(arch) ? IMAGE_FILE_32BIT_MACHINE : 0) |
      IMAGE_FILE_DEBUG_STRIPPED;

    uint32_t string_table_base = file_arena.allocate(string_arena.offset);
    uint32_t rdata_size = rdata_arena.offset;
    uint32_t rdata_base = file_arena.allocate(rdata_arena.offset);

    SectionTableEntry rdata_section = { 0 };
    strcpy(rdata_section.name, ".rdata");
    rdata_section.virtual_size = 0;
    rdata_section.virtual_address = 0;
    rdata_section.size_of_raw_data = rdata_size;
    rdata_section.pointer_to_raw_data = rdata_base;
    rdata_section.pointer_to_relocations = 0;
    rdata_section.pointer_to_line_numbers = 0;
    rdata_section.number_of_relocations = 0;
    rdata_section.number_of_line_numbers = 0;
    rdata_section.characteristics = IMAGE_SCN_CNT_INITIALIZED_DATA |
                                    IMAGE_SCN_MEM_READ | IMAGE_SCN_ALIGN_8BYTES;

    // write file header, section table, symbol table, string table, rdata
    {
      auto&& _ = file_header;
      blit(os, _.machine_type);
      blit(os, _.section_count);
      blit(os, _.time_date_stamp);
      blit(os, _.symbol_table_pointer);
      blit(os, _.symbol_count);
      blit(os, _.optional_header_size);
      blit(os, _.characteristics);
    }
    {
      auto&& _ = rdata_section;
      blit(os, _.name);
      blit(os, _.virtual_size);
      blit(os, _.virtual_address);
      blit(os, _.size_of_raw_data);
      blit(os, _.pointer_to_raw_data);
      blit(os, _.pointer_to_relocations);
      blit(os, _.pointer_to_line_numbers);
      blit(os, _.number_of_relocations);
      blit(os, _.number_of_line_numbers);
      blit(os, _.characteristics);
    }
    for (auto&& _ : symbol_entries) {
      blit(os, _.name);
      blit(os, _.value);
      blit(os, _.section_number);
      blit(os, _.type);
      blit(os, _.storage_class);
      blit(os, _.number_of_aux_symbols);
    }
    os.write(string_data.data(), string_data.size());
    {
      uint32_t cur = 0;
      for (size_t i = 0; i < variables.size(); ++i) {
        if (variable_offsets[i] != cur) {
          auto n = variable_offsets[i] - cur;
          std::vector<char> pad(n);
          os.write(pad.data(), pad.size());
          cur += n;
        }
        variables[i]->write_to_stream(os);
        cur += variables[i]->get_size();
      }
    }
  }

  static uint16_t machine_type(Arch arch)
  {
    switch (arch) {
      case Arch::x86_64:
        return 0x8664;
      case Arch::i386:
        return 0x014c;
      default:
        abort();
    }
  }

  Arch arch;
  std::vector<std::shared_ptr<Variable>> variables;
};

bool
write_coff_file(std::ofstream& out,
                std::ifstream& data,
                uint32_t size,
                std::string base_name)
{
  bool is_32bit = false;

  SpaceAllocator arena;
  arena.allocate(20 + 40);

  uint32_t symbol_table_size = 18 * 2;
  uint32_t symbol_table_offset = arena.allocate(symbol_table_size);

  uint32_t rdata_size = 8 + size;
  uint32_t rdata_offset = arena.aligned_allocate(rdata_size, 8);

  // Write COFF file header.
  {
    uint16_t machine_type =
      is_32bit ? IMAGE_FILE_MACHINE_I386 : IMAGE_FILE_MACHINE_AMD64;
    blit(out, machine_type);
    uint16_t section_count = 1;
    blit(out, section_count);
    uint32_t time_date_stamp = std::time(nullptr);
    blit(out, time_date_stamp);
    uint32_t symbol_table_pointer = symbol_table_offset;
    blit(out, symbol_table_pointer);
    uint32_t symbol_count = 2;
    blit(out, symbol_count);
    uint16_t optional_header_size = 0;
    blit(out, optional_header_size);
    uint16_t characteristics = IMAGE_FILE_LARGE_ADDRESS_AWARE |
                               (is_32bit ? IMAGE_FILE_32BIT_MACHINE : 0) |
                               IMAGE_FILE_DEBUG_STRIPPED;
    blit(out, characteristics);
  }

  // Write rdata section header.
  {
    char sec_name[8] = ".rdata";
    out.write(sec_name, sizeof(sec_name));
    uint32_t virtual_size = 0;
    blit(out, virtual_size);
    uint32_t virtual_address = 0;
    blit(out, virtual_address);
    uint32_t size_of_raw_data = rdata_size;
    blit(out, size_of_raw_data);
    uint32_t pointer_to_raw_data = rdata_offset;
    blit(out, pointer_to_raw_data);
    uint32_t pointer_to_relocations = 0;
    blit(out, pointer_to_relocations);
    uint32_t pointer_to_line_numbers = 0;
    blit(out, pointer_to_line_numbers);
    uint16_t number_of_relocations = 0;
    blit(out, number_of_relocations);
    uint16_t number_of_line_numbers = 0;
    blit(out, number_of_line_numbers);
    uint32_t characteristics = IMAGE_SCN_CNT_INITIALIZED_DATA |
                               IMAGE_SCN_MEM_READ | IMAGE_SCN_ALIGN_8BYTES;
    blit(out, characteristics);
  }

  // Write symbol table.
  uint32_t addresses[] = { 0, 8 };
  std::string names[] = { base_name + "_size", base_name + "_data" };
  for (size_t i = 0; i < 2; ++i) {
    uint8_t name[8] = { 0 };
    if (names[i].size() <= 8) {
      std::copy(names[i].begin(), names[i].end(), name);
    }
    uint32_t value;
    uint16_t section_number = 1;
    uint16_t type = 0x0;
    uint8_t storage_class;
    uint8_t number_of_aux_symbols;
  }

  // Write string table.

  // Write rdata section.
  blit(out, size);
  std::copy_n(std::istreambuf_iterator<char>(data),
              size,
              std::ostreambuf_iterator<char>(out));

  return true;
}

auto
main(int argc, char** argv) -> int
{
  if (argc != 5) {
    std::cerr << g_usage << std::endl;
    return 1;
  }

  auto symbol_base = std::string(argv[1]);
  auto input_filename = std::string(argv[2]);
  auto out_object_filename = std::string(argv[3]);
  auto out_header_filename = std::string(argv[4]);
  std::ifstream is(input_filename, std::ios::binary);
  if (!is) {
    std::cerr << "input file \"" + input_filename + "\" not found\n";
    return 1;
  }

  is.seekg(0, std::ios::end);
  uint32_t cb = (uint32_t)is.tellg();
  is.seekg(0, std::ios::beg);

  auto size_var = std::make_shared<OwnedDataVariable>(
    symbol_base + "_size", &cb, sizeof(uint32_t), 4);
  auto data_var =
    std::make_shared<StreamVariable>(symbol_base + "_data", is, cb, 1);

  CoffFile coff(Arch::x86_64);
  coff.add_variable(size_var);
  coff.add_variable(data_var);

  std::ofstream os(out_object_filename, std::ios::binary);

  if (!os) {
    std::cerr << "object file \"" + out_object_filename + "\" not usable\n";
    return 2;
  }

  coff.write_to_stream(os);
  return 0;
}